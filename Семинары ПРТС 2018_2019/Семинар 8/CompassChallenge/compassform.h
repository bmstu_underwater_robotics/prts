#ifndef COMPASSFORM_H
#define COMPASSFORM_H

#include <QWidget>
#include <QHBoxLayout>
#include <QKeyEvent>

#include "ui_compassform.h"
#include "picframe.h"


class CompassForm : public QWidget, public Ui::CompassForm {
    Q_OBJECT

public:
    explicit CompassForm(QWidget *parent = 0);
    ~CompassForm();


private:

protected:


};

#endif // COMPASSFORM_H
