/********************************************************************************
** Form generated from reading UI file 'cableparams.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CABLEPARAMS_H
#define UI_CABLEPARAMS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CableParams
{
public:
    QVBoxLayout *verticalLayout_5;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *length1;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_3;
    QLineEdit *w1;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_17;
    QLineEdit *diametr;
    QLabel *label_18;
    QHBoxLayout *horizontalLayout_3;
    QCheckBox *enable2;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_9;
    QLineEdit *length2;
    QLabel *label_10;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_11;
    QLineEdit *w2;
    QLabel *label_12;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_21;
    QLineEdit *diametr2;
    QLabel *label_22;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_19;
    QLineEdit *wgarazh;
    QLabel *label_20;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_13;
    QLineEdit *lengthsumm;
    QLabel *label_14;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_15;
    QLineEdit *length;
    QLabel *label_16;

    void setupUi(QWidget *CableParams)
    {
        if (CableParams->objectName().isEmpty())
            CableParams->setObjectName(QStringLiteral("CableParams"));
        CableParams->resize(308, 539);
        CableParams->setMinimumSize(QSize(308, 539));
        verticalLayout_5 = new QVBoxLayout(CableParams);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        groupBox = new QGroupBox(CableParams);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setStyleSheet(QLatin1String("border-color:black;\n"
"border-width: 3px;\n"
""));
        groupBox->setAlignment(Qt::AlignCenter);
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(271, 131));
        verticalLayout_6 = new QVBoxLayout(groupBox_2);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(10);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(10, -1, 10, -1);
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        length1 = new QLineEdit(groupBox_2);
        length1->setObjectName(QStringLiteral("length1"));
        length1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(length1);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(10);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(10, -1, 10, -1);
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_2->addWidget(label_3);

        w1 = new QLineEdit(groupBox_2);
        w1->setObjectName(QStringLiteral("w1"));
        w1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(w1);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_2->addWidget(label_4);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(10);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(10, -1, 10, -1);
        label_17 = new QLabel(groupBox_2);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setMinimumSize(QSize(0, 0));

        horizontalLayout_10->addWidget(label_17);

        diametr = new QLineEdit(groupBox_2);
        diametr->setObjectName(QStringLiteral("diametr"));
        diametr->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_10->addWidget(diametr);

        label_18 = new QLabel(groupBox_2);
        label_18->setObjectName(QStringLiteral("label_18"));

        horizontalLayout_10->addWidget(label_18);


        verticalLayout->addLayout(horizontalLayout_10);


        verticalLayout_6->addLayout(verticalLayout);


        verticalLayout_2->addWidget(groupBox_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(10);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(10, -1, -1, -1);
        enable2 = new QCheckBox(groupBox);
        enable2->setObjectName(QStringLiteral("enable2"));
        enable2->setMinimumSize(QSize(0, 17));

        horizontalLayout_3->addWidget(enable2);


        verticalLayout_2->addLayout(horizontalLayout_3);

        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setEnabled(false);
        groupBox_3->setMinimumSize(QSize(271, 172));
        verticalLayout_7 = new QVBoxLayout(groupBox_3);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(10);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(10, -1, 10, -1);
        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_6->addWidget(label_9);

        length2 = new QLineEdit(groupBox_3);
        length2->setObjectName(QStringLiteral("length2"));
        length2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(length2);

        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout_6->addWidget(label_10);


        verticalLayout_3->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(10);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(10, -1, 10, -1);
        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QStringLiteral("label_11"));

        horizontalLayout_7->addWidget(label_11);

        w2 = new QLineEdit(groupBox_3);
        w2->setObjectName(QStringLiteral("w2"));
        w2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_7->addWidget(w2);

        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QStringLiteral("label_12"));

        horizontalLayout_7->addWidget(label_12);


        verticalLayout_3->addLayout(horizontalLayout_7);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(10);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(10, -1, 10, -1);
        label_21 = new QLabel(groupBox_3);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setMinimumSize(QSize(0, 0));

        horizontalLayout_12->addWidget(label_21);

        diametr2 = new QLineEdit(groupBox_3);
        diametr2->setObjectName(QStringLiteral("diametr2"));
        diametr2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_12->addWidget(diametr2);

        label_22 = new QLabel(groupBox_3);
        label_22->setObjectName(QStringLiteral("label_22"));

        horizontalLayout_12->addWidget(label_22);


        verticalLayout_3->addLayout(horizontalLayout_12);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(10);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(10, -1, 10, -1);
        label_19 = new QLabel(groupBox_3);
        label_19->setObjectName(QStringLiteral("label_19"));

        horizontalLayout_11->addWidget(label_19);

        wgarazh = new QLineEdit(groupBox_3);
        wgarazh->setObjectName(QStringLiteral("wgarazh"));
        wgarazh->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_11->addWidget(wgarazh);

        label_20 = new QLabel(groupBox_3);
        label_20->setObjectName(QStringLiteral("label_20"));

        horizontalLayout_11->addWidget(label_20);


        verticalLayout_3->addLayout(horizontalLayout_11);


        verticalLayout_7->addLayout(verticalLayout_3);


        verticalLayout_2->addWidget(groupBox_3);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(10);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(10, -1, 10, -1);
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setMinimumSize(QSize(0, 39));
        label_13->setMaximumSize(QSize(16777215, 39));

        horizontalLayout_8->addWidget(label_13);

        lengthsumm = new QLineEdit(groupBox);
        lengthsumm->setObjectName(QStringLiteral("lengthsumm"));
        lengthsumm->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lengthsumm->setReadOnly(true);

        horizontalLayout_8->addWidget(lengthsumm);

        label_14 = new QLabel(groupBox);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_8->addWidget(label_14);


        verticalLayout_2->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(10);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(10, -1, 10, -1);
        label_15 = new QLabel(groupBox);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setMinimumSize(QSize(0, 39));
        label_15->setMaximumSize(QSize(16777215, 39));

        horizontalLayout_9->addWidget(label_15);

        length = new QLineEdit(groupBox);
        length->setObjectName(QStringLiteral("length"));
        length->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(length);

        label_16 = new QLabel(groupBox);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_9->addWidget(label_16);


        verticalLayout_2->addLayout(horizontalLayout_9);


        verticalLayout_4->addLayout(verticalLayout_2);


        verticalLayout_5->addWidget(groupBox);


        retranslateUi(CableParams);

        QMetaObject::connectSlotsByName(CableParams);
    } // setupUi

    void retranslateUi(QWidget *CableParams)
    {
        CableParams->setWindowTitle(QApplication::translate("CableParams", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("CableParams", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213 \320\272\320\260\320\261\320\265\320\273\321\217", nullptr));
        groupBox_2->setTitle(QApplication::translate("CableParams", "\320\243\321\207\320\260\321\201\321\202\320\276\320\272 1", nullptr));
        label->setText(QApplication::translate("CableParams", "\320\224\320\273\320\270\320\275\320\260 \320\272\320\260\320\261\320\265\320\273\321\217", nullptr));
        label_2->setText(QApplication::translate("CableParams", "\320\274", nullptr));
        label_3->setText(QApplication::translate("CableParams", "\320\237\320\276\320\263\320\276\320\275\320\275\320\260\321\217 \320\274\320\260\321\201\321\201\320\260", nullptr));
        label_4->setText(QApplication::translate("CableParams", "\320\272\320\263/\320\274", nullptr));
        label_17->setText(QApplication::translate("CableParams", "\320\224\320\270\320\260\320\274\320\265\321\202\321\200 \320\272\320\260\320\261\320\265\320\273\321\217", nullptr));
        label_18->setText(QApplication::translate("CableParams", "\320\274\320\274", nullptr));
        enable2->setText(QApplication::translate("CableParams", "\320\222\321\202\320\276\321\200\320\276\320\271 \321\203\321\207\320\260\321\201\321\202\320\276\320\272", nullptr));
        groupBox_3->setTitle(QApplication::translate("CableParams", "\320\243\321\207\320\260\321\201\321\202\320\276\320\272 2", nullptr));
        label_9->setText(QApplication::translate("CableParams", "\320\224\320\273\320\270\320\275\320\260 \320\272\320\260\320\261\320\265\320\273\321\217", nullptr));
        label_10->setText(QApplication::translate("CableParams", "\320\274", nullptr));
        label_11->setText(QApplication::translate("CableParams", "\320\237\320\276\320\263\320\276\320\275\320\275\320\260\321\217 \320\274\320\260\321\201\321\201\320\260", nullptr));
        label_12->setText(QApplication::translate("CableParams", "\320\272\320\263/\320\274", nullptr));
        label_21->setText(QApplication::translate("CableParams", "\320\224\320\270\320\260\320\274\320\265\321\202\321\200 \320\272\320\260\320\261\320\265\320\273\321\217", nullptr));
        label_22->setText(QApplication::translate("CableParams", "\320\274\320\274", nullptr));
        label_19->setText(QApplication::translate("CableParams", "\320\237\320\273\320\260\320\262\321\203\321\207\320\265\321\201\321\202\321\214 \"\320\263\320\260\321\200\320\260\320\266\320\260\"", nullptr));
        label_20->setText(QApplication::translate("CableParams", "\320\235", nullptr));
        label_13->setText(QApplication::translate("CableParams", "\320\236\320\261\321\211\320\260\321\217 \320\264\320\273\320\270\320\275\320\260 \320\272\320\260\320\261\320\265\320\273\321\217", nullptr));
        label_14->setText(QApplication::translate("CableParams", "\320\274", nullptr));
        label_15->setText(QApplication::translate("CableParams", "\320\224\320\273\320\270\320\275\320\260 \320\276\321\202\321\200\320\265\320\267\320\272\320\260 \320\272\320\260\320\261\320\265\320\273\321\217", nullptr));
        label_16->setText(QApplication::translate("CableParams", "\320\274", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CableParams: public Ui_CableParams {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CABLEPARAMS_H
