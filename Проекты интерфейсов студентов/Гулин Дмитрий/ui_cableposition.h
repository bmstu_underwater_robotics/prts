/********************************************************************************
** Form generated from reading UI file 'cableposition.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CABLEPOSITION_H
#define UI_CABLEPOSITION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "cableposition_1.h"
#include "cableposition_table.h"
#include "graphics.h"

QT_BEGIN_NAMESPACE

class Ui_CablePosition
{
public:
    QVBoxLayout *verticalLayout_10;
    QVBoxLayout *verticalLayout;
    cableposition_1 *widget1;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *L1minus;
    QToolButton *L1zero;
    QToolButton *L1plus;
    QHBoxLayout *horizontalLayout_3;
    QToolButton *L1stepminus;
    QLabel *L1step;
    QToolButton *L1stepplus;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_4;
    QToolButton *L2minus;
    QToolButton *L2zero;
    QToolButton *L2plus;
    QHBoxLayout *horizontalLayout_5;
    QToolButton *L2stepminus;
    QLabel *L2step;
    QToolButton *L2stepplus;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_6;
    QToolButton *Pxminus;
    QToolButton *Pxzero;
    QToolButton *Pxplus;
    QHBoxLayout *horizontalLayout_7;
    QToolButton *Pxstepminus;
    QLabel *Pxstep;
    QToolButton *Pxstepplus;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_8;
    QToolButton *Pyminus;
    QToolButton *Pyzero;
    QToolButton *Pyplus;
    QHBoxLayout *horizontalLayout_9;
    QToolButton *Pystepminus;
    QLabel *Pystep;
    QToolButton *Pystepplus;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_10;
    QToolButton *Vxminus;
    QToolButton *Vxzero;
    QToolButton *Vxplus;
    QHBoxLayout *horizontalLayout_11;
    QToolButton *Vxstepminus;
    QLabel *Vxstep;
    QToolButton *Vxstepplus;
    QVBoxLayout *verticalLayout_9;
    QToolButton *toolButton_2;
    Graphics *widget_3;
    QVBoxLayout *verticalLayout_8;
    QToolButton *toolButton;
    cableposition_table *widget_2;

    void setupUi(QWidget *CablePosition)
    {
        if (CablePosition->objectName().isEmpty())
            CablePosition->setObjectName(QStringLiteral("CablePosition"));
        CablePosition->resize(743, 577);
        CablePosition->setStyleSheet(QStringLiteral(""));
        verticalLayout_10 = new QVBoxLayout(CablePosition);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        widget1 = new cableposition_1(CablePosition);
        widget1->setObjectName(QStringLiteral("widget1"));
        widget1->setMinimumSize(QSize(0, 45));
        widget1->setMaximumSize(QSize(16777215, 45));
        widget1->setStyleSheet(QStringLiteral("border: 1px solid grey;"));

        verticalLayout->addWidget(widget1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(10);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        groupBox = new QGroupBox(CablePosition);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setStyleSheet(QLatin1String("background-color: lightblue;\n"
"border:1px solid gray;"));
        groupBox->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        groupBox->setFlat(false);
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        L1minus = new QToolButton(groupBox);
        L1minus->setObjectName(QStringLiteral("L1minus"));
        L1minus->setMinimumSize(QSize(40, 30));
        L1minus->setMaximumSize(QSize(40, 30));
        L1minus->setArrowType(Qt::NoArrow);

        horizontalLayout_2->addWidget(L1minus);

        L1zero = new QToolButton(groupBox);
        L1zero->setObjectName(QStringLiteral("L1zero"));
        L1zero->setMinimumSize(QSize(40, 30));
        L1zero->setMaximumSize(QSize(40, 30));
        L1zero->setArrowType(Qt::NoArrow);

        horizontalLayout_2->addWidget(L1zero);

        L1plus = new QToolButton(groupBox);
        L1plus->setObjectName(QStringLiteral("L1plus"));
        L1plus->setMinimumSize(QSize(40, 30));
        L1plus->setMaximumSize(QSize(40, 30));
        L1plus->setArrowType(Qt::NoArrow);

        horizontalLayout_2->addWidget(L1plus);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        L1stepminus = new QToolButton(groupBox);
        L1stepminus->setObjectName(QStringLiteral("L1stepminus"));
        L1stepminus->setMinimumSize(QSize(40, 30));
        L1stepminus->setMaximumSize(QSize(40, 30));
        L1stepminus->setArrowType(Qt::NoArrow);

        horizontalLayout_3->addWidget(L1stepminus);

        L1step = new QLabel(groupBox);
        L1step->setObjectName(QStringLiteral("L1step"));
        L1step->setMinimumSize(QSize(40, 30));
        L1step->setMaximumSize(QSize(40, 30));
        L1step->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(L1step);

        L1stepplus = new QToolButton(groupBox);
        L1stepplus->setObjectName(QStringLiteral("L1stepplus"));
        L1stepplus->setMinimumSize(QSize(40, 30));
        L1stepplus->setMaximumSize(QSize(40, 30));
        L1stepplus->setArrowType(Qt::NoArrow);

        horizontalLayout_3->addWidget(L1stepplus);


        verticalLayout_2->addLayout(horizontalLayout_3);


        verticalLayout_3->addWidget(groupBox);

        groupBox_2 = new QGroupBox(CablePosition);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setStyleSheet(QLatin1String("background-color: lightgreen;\n"
"border:1px solid gray;"));
        groupBox_2->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        groupBox_2->setFlat(false);
        verticalLayout_4 = new QVBoxLayout(groupBox_2);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        L2minus = new QToolButton(groupBox_2);
        L2minus->setObjectName(QStringLiteral("L2minus"));
        L2minus->setMinimumSize(QSize(40, 30));
        L2minus->setMaximumSize(QSize(40, 30));
        L2minus->setArrowType(Qt::NoArrow);

        horizontalLayout_4->addWidget(L2minus);

        L2zero = new QToolButton(groupBox_2);
        L2zero->setObjectName(QStringLiteral("L2zero"));
        L2zero->setMinimumSize(QSize(40, 30));
        L2zero->setMaximumSize(QSize(40, 30));
        L2zero->setArrowType(Qt::NoArrow);

        horizontalLayout_4->addWidget(L2zero);

        L2plus = new QToolButton(groupBox_2);
        L2plus->setObjectName(QStringLiteral("L2plus"));
        L2plus->setMinimumSize(QSize(40, 30));
        L2plus->setMaximumSize(QSize(40, 30));
        L2plus->setArrowType(Qt::NoArrow);

        horizontalLayout_4->addWidget(L2plus);


        verticalLayout_4->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        L2stepminus = new QToolButton(groupBox_2);
        L2stepminus->setObjectName(QStringLiteral("L2stepminus"));
        L2stepminus->setMinimumSize(QSize(40, 30));
        L2stepminus->setMaximumSize(QSize(40, 30));
        L2stepminus->setArrowType(Qt::NoArrow);

        horizontalLayout_5->addWidget(L2stepminus);

        L2step = new QLabel(groupBox_2);
        L2step->setObjectName(QStringLiteral("L2step"));
        L2step->setMinimumSize(QSize(40, 30));
        L2step->setMaximumSize(QSize(40, 30));
        L2step->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(L2step);

        L2stepplus = new QToolButton(groupBox_2);
        L2stepplus->setObjectName(QStringLiteral("L2stepplus"));
        L2stepplus->setMinimumSize(QSize(40, 30));
        L2stepplus->setMaximumSize(QSize(40, 30));
        L2stepplus->setArrowType(Qt::NoArrow);

        horizontalLayout_5->addWidget(L2stepplus);


        verticalLayout_4->addLayout(horizontalLayout_5);


        verticalLayout_3->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(CablePosition);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setStyleSheet(QLatin1String("background-color: white;\n"
"border:1px solid gray;"));
        groupBox_3->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        groupBox_3->setFlat(false);
        verticalLayout_5 = new QVBoxLayout(groupBox_3);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        Pxminus = new QToolButton(groupBox_3);
        Pxminus->setObjectName(QStringLiteral("Pxminus"));
        Pxminus->setMinimumSize(QSize(40, 30));
        Pxminus->setMaximumSize(QSize(40, 30));
        Pxminus->setArrowType(Qt::NoArrow);

        horizontalLayout_6->addWidget(Pxminus);

        Pxzero = new QToolButton(groupBox_3);
        Pxzero->setObjectName(QStringLiteral("Pxzero"));
        Pxzero->setMinimumSize(QSize(40, 30));
        Pxzero->setMaximumSize(QSize(40, 30));
        Pxzero->setArrowType(Qt::NoArrow);

        horizontalLayout_6->addWidget(Pxzero);

        Pxplus = new QToolButton(groupBox_3);
        Pxplus->setObjectName(QStringLiteral("Pxplus"));
        Pxplus->setMinimumSize(QSize(40, 30));
        Pxplus->setMaximumSize(QSize(40, 30));
        Pxplus->setArrowType(Qt::NoArrow);

        horizontalLayout_6->addWidget(Pxplus);


        verticalLayout_5->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        Pxstepminus = new QToolButton(groupBox_3);
        Pxstepminus->setObjectName(QStringLiteral("Pxstepminus"));
        Pxstepminus->setMinimumSize(QSize(40, 30));
        Pxstepminus->setMaximumSize(QSize(40, 30));
        Pxstepminus->setArrowType(Qt::NoArrow);

        horizontalLayout_7->addWidget(Pxstepminus);

        Pxstep = new QLabel(groupBox_3);
        Pxstep->setObjectName(QStringLiteral("Pxstep"));
        Pxstep->setMinimumSize(QSize(40, 30));
        Pxstep->setMaximumSize(QSize(40, 30));
        Pxstep->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(Pxstep);

        Pxstepplus = new QToolButton(groupBox_3);
        Pxstepplus->setObjectName(QStringLiteral("Pxstepplus"));
        Pxstepplus->setMinimumSize(QSize(40, 30));
        Pxstepplus->setMaximumSize(QSize(40, 30));
        Pxstepplus->setArrowType(Qt::NoArrow);

        horizontalLayout_7->addWidget(Pxstepplus);


        verticalLayout_5->addLayout(horizontalLayout_7);


        verticalLayout_3->addWidget(groupBox_3);

        groupBox_4 = new QGroupBox(CablePosition);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setStyleSheet(QLatin1String("background-color: white;\n"
"border:1px solid gray;"));
        groupBox_4->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        groupBox_4->setFlat(false);
        verticalLayout_6 = new QVBoxLayout(groupBox_4);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        Pyminus = new QToolButton(groupBox_4);
        Pyminus->setObjectName(QStringLiteral("Pyminus"));
        Pyminus->setMinimumSize(QSize(40, 30));
        Pyminus->setMaximumSize(QSize(40, 30));
        Pyminus->setArrowType(Qt::NoArrow);

        horizontalLayout_8->addWidget(Pyminus);

        Pyzero = new QToolButton(groupBox_4);
        Pyzero->setObjectName(QStringLiteral("Pyzero"));
        Pyzero->setMinimumSize(QSize(40, 30));
        Pyzero->setMaximumSize(QSize(40, 30));
        Pyzero->setArrowType(Qt::NoArrow);

        horizontalLayout_8->addWidget(Pyzero);

        Pyplus = new QToolButton(groupBox_4);
        Pyplus->setObjectName(QStringLiteral("Pyplus"));
        Pyplus->setMinimumSize(QSize(40, 30));
        Pyplus->setMaximumSize(QSize(40, 30));
        Pyplus->setArrowType(Qt::NoArrow);

        horizontalLayout_8->addWidget(Pyplus);


        verticalLayout_6->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        Pystepminus = new QToolButton(groupBox_4);
        Pystepminus->setObjectName(QStringLiteral("Pystepminus"));
        Pystepminus->setMinimumSize(QSize(40, 30));
        Pystepminus->setMaximumSize(QSize(40, 30));
        Pystepminus->setArrowType(Qt::NoArrow);

        horizontalLayout_9->addWidget(Pystepminus);

        Pystep = new QLabel(groupBox_4);
        Pystep->setObjectName(QStringLiteral("Pystep"));
        Pystep->setMinimumSize(QSize(40, 30));
        Pystep->setMaximumSize(QSize(40, 30));
        Pystep->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(Pystep);

        Pystepplus = new QToolButton(groupBox_4);
        Pystepplus->setObjectName(QStringLiteral("Pystepplus"));
        Pystepplus->setMinimumSize(QSize(40, 30));
        Pystepplus->setMaximumSize(QSize(40, 30));
        Pystepplus->setArrowType(Qt::NoArrow);

        horizontalLayout_9->addWidget(Pystepplus);


        verticalLayout_6->addLayout(horizontalLayout_9);


        verticalLayout_3->addWidget(groupBox_4);

        groupBox_5 = new QGroupBox(CablePosition);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setStyleSheet(QLatin1String("background-color: white;\n"
"border:1px solid gray;"));
        groupBox_5->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        groupBox_5->setFlat(false);
        verticalLayout_7 = new QVBoxLayout(groupBox_5);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        Vxminus = new QToolButton(groupBox_5);
        Vxminus->setObjectName(QStringLiteral("Vxminus"));
        Vxminus->setMinimumSize(QSize(40, 30));
        Vxminus->setMaximumSize(QSize(40, 30));
        Vxminus->setArrowType(Qt::NoArrow);

        horizontalLayout_10->addWidget(Vxminus);

        Vxzero = new QToolButton(groupBox_5);
        Vxzero->setObjectName(QStringLiteral("Vxzero"));
        Vxzero->setMinimumSize(QSize(40, 30));
        Vxzero->setMaximumSize(QSize(40, 30));
        Vxzero->setArrowType(Qt::NoArrow);

        horizontalLayout_10->addWidget(Vxzero);

        Vxplus = new QToolButton(groupBox_5);
        Vxplus->setObjectName(QStringLiteral("Vxplus"));
        Vxplus->setMinimumSize(QSize(40, 30));
        Vxplus->setMaximumSize(QSize(40, 30));
        Vxplus->setArrowType(Qt::NoArrow);

        horizontalLayout_10->addWidget(Vxplus);


        verticalLayout_7->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        Vxstepminus = new QToolButton(groupBox_5);
        Vxstepminus->setObjectName(QStringLiteral("Vxstepminus"));
        Vxstepminus->setMinimumSize(QSize(40, 30));
        Vxstepminus->setMaximumSize(QSize(40, 30));
        Vxstepminus->setArrowType(Qt::NoArrow);

        horizontalLayout_11->addWidget(Vxstepminus);

        Vxstep = new QLabel(groupBox_5);
        Vxstep->setObjectName(QStringLiteral("Vxstep"));
        Vxstep->setMinimumSize(QSize(40, 30));
        Vxstep->setMaximumSize(QSize(40, 30));
        Vxstep->setAlignment(Qt::AlignCenter);

        horizontalLayout_11->addWidget(Vxstep);

        Vxstepplus = new QToolButton(groupBox_5);
        Vxstepplus->setObjectName(QStringLiteral("Vxstepplus"));
        Vxstepplus->setMinimumSize(QSize(40, 30));
        Vxstepplus->setMaximumSize(QSize(40, 30));
        Vxstepplus->setArrowType(Qt::NoArrow);

        horizontalLayout_11->addWidget(Vxstepplus);


        verticalLayout_7->addLayout(horizontalLayout_11);


        verticalLayout_3->addWidget(groupBox_5);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setSpacing(0);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        toolButton_2 = new QToolButton(CablePosition);
        toolButton_2->setObjectName(QStringLiteral("toolButton_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(toolButton_2->sizePolicy().hasHeightForWidth());
        toolButton_2->setSizePolicy(sizePolicy);
        toolButton_2->setMinimumSize(QSize(20, 0));
        toolButton_2->setMaximumSize(QSize(20, 16777215));
        toolButton_2->setArrowType(Qt::LeftArrow);

        verticalLayout_9->addWidget(toolButton_2);


        horizontalLayout->addLayout(verticalLayout_9);

        widget_3 = new Graphics(CablePosition);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        widget_3->setStyleSheet(QLatin1String("border:1px solid grey;\n"
""));

        horizontalLayout->addWidget(widget_3);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(0);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        toolButton = new QToolButton(CablePosition);
        toolButton->setObjectName(QStringLiteral("toolButton"));
        sizePolicy.setHeightForWidth(toolButton->sizePolicy().hasHeightForWidth());
        toolButton->setSizePolicy(sizePolicy);
        toolButton->setMinimumSize(QSize(20, 0));
        toolButton->setMaximumSize(QSize(20, 16777215));
        toolButton->setArrowType(Qt::RightArrow);

        verticalLayout_8->addWidget(toolButton);


        horizontalLayout->addLayout(verticalLayout_8);

        widget_2 = new cableposition_table(CablePosition);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        widget_2->setMinimumSize(QSize(255, 0));
        widget_2->setMaximumSize(QSize(255, 16777215));
        widget_2->setStyleSheet(QStringLiteral(""));

        horizontalLayout->addWidget(widget_2);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_10->addLayout(verticalLayout);


        retranslateUi(CablePosition);

        QMetaObject::connectSlotsByName(CablePosition);
    } // setupUi

    void retranslateUi(QWidget *CablePosition)
    {
        CablePosition->setWindowTitle(QApplication::translate("CablePosition", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("CablePosition", "L1", nullptr));
        L1minus->setText(QApplication::translate("CablePosition", "-", nullptr));
        L1zero->setText(QApplication::translate("CablePosition", "0", nullptr));
        L1plus->setText(QApplication::translate("CablePosition", "+", nullptr));
        L1stepminus->setText(QApplication::translate("CablePosition", "0.1x", nullptr));
        L1step->setText(QApplication::translate("CablePosition", "10", nullptr));
        L1stepplus->setText(QApplication::translate("CablePosition", "10x", nullptr));
        groupBox_2->setTitle(QApplication::translate("CablePosition", "L2", nullptr));
        L2minus->setText(QApplication::translate("CablePosition", "-", nullptr));
        L2zero->setText(QApplication::translate("CablePosition", "0", nullptr));
        L2plus->setText(QApplication::translate("CablePosition", "+", nullptr));
        L2stepminus->setText(QApplication::translate("CablePosition", "0.1x", nullptr));
        L2step->setText(QApplication::translate("CablePosition", "10", nullptr));
        L2stepplus->setText(QApplication::translate("CablePosition", "10x", nullptr));
        groupBox_3->setTitle(QApplication::translate("CablePosition", "Px", nullptr));
        Pxminus->setText(QApplication::translate("CablePosition", "-", nullptr));
        Pxzero->setText(QApplication::translate("CablePosition", "0", nullptr));
        Pxplus->setText(QApplication::translate("CablePosition", "+", nullptr));
        Pxstepminus->setText(QApplication::translate("CablePosition", "0.1x", nullptr));
        Pxstep->setText(QApplication::translate("CablePosition", "10", nullptr));
        Pxstepplus->setText(QApplication::translate("CablePosition", "10x", nullptr));
        groupBox_4->setTitle(QApplication::translate("CablePosition", "Py", nullptr));
        Pyminus->setText(QApplication::translate("CablePosition", "-", nullptr));
        Pyzero->setText(QApplication::translate("CablePosition", "0", nullptr));
        Pyplus->setText(QApplication::translate("CablePosition", "+", nullptr));
        Pystepminus->setText(QApplication::translate("CablePosition", "0.1x", nullptr));
        Pystep->setText(QApplication::translate("CablePosition", "10", nullptr));
        Pystepplus->setText(QApplication::translate("CablePosition", "10x", nullptr));
        groupBox_5->setTitle(QApplication::translate("CablePosition", "Vx", nullptr));
        Vxminus->setText(QApplication::translate("CablePosition", "-", nullptr));
        Vxzero->setText(QApplication::translate("CablePosition", "0", nullptr));
        Vxplus->setText(QApplication::translate("CablePosition", "+", nullptr));
        Vxstepminus->setText(QApplication::translate("CablePosition", "0.1x", nullptr));
        Vxstep->setText(QApplication::translate("CablePosition", "0.1", nullptr));
        Vxstepplus->setText(QApplication::translate("CablePosition", "10x", nullptr));
        toolButton_2->setText(QApplication::translate("CablePosition", "...", nullptr));
        toolButton->setText(QApplication::translate("CablePosition", "...", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CablePosition: public Ui_CablePosition {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CABLEPOSITION_H
