#-------------------------------------------------
#
# Project created by QtCreator 2018-10-23T14:11:05
#
#-------------------------------------------------

QT       += core gui widgets charts

TARGET = CableCalculation
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        cablecalculation.cpp \
    cableparams.cpp \
    tnpaparams.cpp \
    buttons.cpp \
    cableposition.cpp \
    cableposition_1.cpp \
    cableposition_table.cpp \
    graphics.cpp

HEADERS += \
        cablecalculation.h \
    cableparams.h \
    tnpaparams.h \
    buttons.h \
    cableposition.h \
    cableposition_1.h \
    cableposition_table.h \
    graphics.h

FORMS += \
        cablecalculation.ui \
    cableparams.ui \
    tnpaparams.ui \
    buttons.ui \
    cableposition.ui \
    cableposition_1.ui \
    cableposition_table.ui \
    graphics.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
