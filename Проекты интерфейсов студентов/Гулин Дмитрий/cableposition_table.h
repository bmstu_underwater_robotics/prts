#ifndef CABLEPOSITION_TABLE_H
#define CABLEPOSITION_TABLE_H

#include "ui_cableposition_table.h"

class cableposition_table : public QWidget, private Ui::cableposition_table
{
    Q_OBJECT

public:
    int count1, count2, count;
    double X, Y;
    double x_max, x_min, y_max, y_min;
    bool en2;
    explicit cableposition_table(QWidget *parent = nullptr);
    void TableUpdate1();
    void TableUpdate2();
    void UpdateData1();
    void UpdateData2();
    double GetData(int i, int j);

};

#endif // CABLEPOSITION_TABLE_H
