#ifndef CABLEPOSITION_H
#define CABLEPOSITION_H

#include "ui_cableposition.h"
#include <QPainter>
#include <QCursor>

class CablePosition : public QWidget, private Ui::CablePosition
{
    Q_OBJECT

public:
    explicit CablePosition(QWidget *parent = nullptr);
    double l1, l2, w1, w2, lsumm, lotr, diam, diam2, wgarazh;
    bool en2 = 0;
    bool en_repain = 1;
    double Px, Py, Vx, WTNPA, Cvx1, Cvx2, max_Depth, Vx_TNPA, Px_max, Py_max;
    double x_max, x_min, y_max, y_min;
    bool en_left = 1;
    bool en_right = 1;

    void TitleSettings();

    void startCalculation();

    void Calculation1();
    void Calculation2();
    void Paint1();

    void UpdateGraph();

public slots:
    void GetData1(int i);
    void EndPaint(bool a);

protected:
  //  void paintEvent(QPaintEvent *e);

private slots:

    void on_L1stepminus_clicked();
    void on_L1stepplus_clicked();
    void on_L2stepminus_clicked();
    void on_L2stepplus_clicked();
    void on_Pxstepminus_clicked();
    void on_Pxstepplus_clicked();
    void on_Pystepminus_clicked();
    void on_Pystepplus_clicked();
    void on_Vxstepminus_clicked();
    void on_Vxstepplus_clicked();
    void on_L1minus_clicked();
    void on_L1zero_clicked();
    void on_L1plus_clicked();
    void on_L2minus_clicked();
    void on_L2zero_clicked();
    void on_L2plus_clicked();
    void on_Pxminus_clicked();
    void on_Pxzero_clicked();
    void on_Pxplus_clicked();
    void on_Pyminus_clicked();
    void on_Pyzero_clicked();
    void on_Pyplus_clicked();
    void on_Vxminus_clicked();
    void on_Vxzero_clicked();
    void on_Vxplus_clicked();
    void on_toolButton_2_clicked();
    void on_toolButton_clicked();
};

#endif // CABLEPOSITION_H
