#include "cableposition_1.h"

cableposition_1::cableposition_1(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
}

void cableposition_1::TitleSettings()
{
    L1label->setText("L1 = "+QString::number(L1)+" м");
    L2label->setText("L2 = "+QString::number(L2)+" м");
    Pxlabel->setText("Px = "+QString::number(Px)+" Н");
    Pylabel->setText("Py = "+QString::number(Py)+" Н");
    Vxlabel->setText("Vx = "+QString::number(Vx)+" м/с");
}
