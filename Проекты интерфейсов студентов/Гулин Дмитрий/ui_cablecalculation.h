/********************************************************************************
** Form generated from reading UI file 'cablecalculation.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CABLECALCULATION_H
#define UI_CABLECALCULATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "buttons.h"
#include "cableparams.h"
#include "tnpaparams.h"

QT_BEGIN_NAMESPACE

class Ui_CableCalculation
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    CableParams *widget;
    TNPAParams *widget_2;
    Buttons *widget_3;

    void setupUi(QWidget *CableCalculation)
    {
        if (CableCalculation->objectName().isEmpty())
            CableCalculation->setObjectName(QStringLiteral("CableCalculation"));
        CableCalculation->resize(644, 629);
        CableCalculation->setMinimumSize(QSize(0, 0));
        CableCalculation->setMaximumSize(QSize(16777215, 16777215));
        verticalLayout_2 = new QVBoxLayout(CableCalculation);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        widget = new CableParams(CableCalculation);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setMinimumSize(QSize(308, 539));
        widget->setTabletTracking(false);
        widget->setAutoFillBackground(false);

        horizontalLayout->addWidget(widget);

        widget_2 = new TNPAParams(CableCalculation);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        widget_2->setMinimumSize(QSize(308, 539));

        horizontalLayout->addWidget(widget_2);


        verticalLayout->addLayout(horizontalLayout);

        widget_3 = new Buttons(CableCalculation);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        widget_3->setMinimumSize(QSize(620, 62));

        verticalLayout->addWidget(widget_3);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(CableCalculation);

        QMetaObject::connectSlotsByName(CableCalculation);
    } // setupUi

    void retranslateUi(QWidget *CableCalculation)
    {
        CableCalculation->setWindowTitle(QApplication::translate("CableCalculation", "CableCalculation", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CableCalculation: public Ui_CableCalculation {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CABLECALCULATION_H
