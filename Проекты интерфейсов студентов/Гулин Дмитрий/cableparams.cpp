#include "cableparams.h"
#include "buttons.h"

CableParams::CableParams(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    k1 = k2 = ksumm = kotr = kw1 = kw2 = diam = garazh = diam2 = 0;
}

void CableParams::on_enable2_stateChanged(int arg1)
{
    groupBox_3->setEnabled(arg1);
    doublecable = arg1;
    emit on_length1_editingFinished();
}

void CableParams::on_length1_editingFinished()
{
    if (doublecable)
        lengthsumm->setText(QString::number(length1->text().toDouble()+length2->text().toDouble()));
    else
        lengthsumm->setText(QString::number(length1->text().toDouble()));
    k1=length1->text().toDouble();
    ksumm = lengthsumm->text().toDouble();
}

void CableParams::on_length2_editingFinished()
{
    lengthsumm->setText(QString::number(length1->text().toDouble()+length2->text().toDouble()));
    k2=length2->text().toDouble();
    ksumm = lengthsumm->text().toDouble();
}

void CableParams::on_w1_editingFinished()
{
    kw1=w1->text().toDouble();
}

void CableParams::on_w2_editingFinished()
{
    kw2=w2->text().toDouble();
}

void CableParams::on_diametr_editingFinished()
{
    diam=diametr->text().toDouble();
}

void CableParams::on_length_editingFinished()
{
    kotr=length->text().toDouble();
}

void CableParams::on_wgarazh_editingFinished()
{
    garazh = wgarazh->text().toDouble();
}

void CableParams::on_diametr2_editingFinished()
{
    diam2=diametr2->text().toDouble();
}
