#ifndef TNPAPARAMS_H
#define TNPAPARAMS_H

#include "ui_tnpaparams.h"

class TNPAParams : public QWidget, private Ui::TNPAParams
{
    Q_OBJECT

public:
    explicit TNPAParams(QWidget *parent = nullptr);
    double zPx, zPy, zPx_max, zPy_max, zWTNPA, zCvx1, zCvx2, zVx_TNPA, zDepth;
private slots:
    void on_Px_editingFinished();
    void on_Py_editingFinished();
    void on_Px_max_editingFinished();
    void on_Py_max_editingFinished();
    void on_WTNPA_editingFinished();
    void on_Cvx1_editingFinished();
    void on_Cvx2_editingFinished();
    void on_Max_depth_editingFinished();
    void on_Vx_editingFinished();
};

#endif // TNPAPARAMS_H
