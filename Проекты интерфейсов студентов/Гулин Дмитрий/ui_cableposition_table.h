/********************************************************************************
** Form generated from reading UI file 'cableposition_table.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CABLEPOSITION_TABLE_H
#define UI_CABLEPOSITION_TABLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_cableposition_table
{
public:
    QHBoxLayout *horizontalLayout;
    QTableWidget *tableWidget;

    void setupUi(QWidget *cableposition_table)
    {
        if (cableposition_table->objectName().isEmpty())
            cableposition_table->setObjectName(QStringLiteral("cableposition_table"));
        cableposition_table->resize(250, 369);
        horizontalLayout = new QHBoxLayout(cableposition_table);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        tableWidget = new QTableWidget(cableposition_table);
        if (tableWidget->columnCount() < 2)
            tableWidget->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        if (tableWidget->rowCount() < 1)
            tableWidget->setRowCount(1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(0, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setItem(0, 0, __qtablewidgetitem3);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setStyleSheet(QLatin1String("QHeaderView::section {\n"
"	spacing: 10px;\n"
"	background-	color:lightgrey;\n"
"	color: black;\n"
"	border: 1px solid grey;\n"
"	margin: 1px;\n"
"	text-align: right;\n"
"	font-family: arial;\n"
"	font-size:16px;\n"
"}\n"
""));

        horizontalLayout->addWidget(tableWidget);


        retranslateUi(cableposition_table);

        QMetaObject::connectSlotsByName(cableposition_table);
    } // setupUi

    void retranslateUi(QWidget *cableposition_table)
    {
        cableposition_table->setWindowTitle(QApplication::translate("cableposition_table", "Form", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("cableposition_table", "X", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("cableposition_table", "Y", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->verticalHeaderItem(0);
        ___qtablewidgetitem2->setText(QApplication::translate("cableposition_table", "1", nullptr));

        const bool __sortingEnabled = tableWidget->isSortingEnabled();
        tableWidget->setSortingEnabled(false);
        tableWidget->setSortingEnabled(__sortingEnabled);

    } // retranslateUi

};

namespace Ui {
    class cableposition_table: public Ui_cableposition_table {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CABLEPOSITION_TABLE_H
