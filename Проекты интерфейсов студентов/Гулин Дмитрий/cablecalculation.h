#ifndef CABLECALCULATION_H
#define CABLECALCULATION_H

#include "ui_cablecalculation.h"
#include <QtCharts/QAbstractSeries>

class CableCalculation : public QWidget, private Ui::CableCalculation
{
    Q_OBJECT

public:
    explicit CableCalculation(QWidget *parent = nullptr);
public slots:
    void button1_clicked_1();
};

#endif // CABLECALCULATION_H
