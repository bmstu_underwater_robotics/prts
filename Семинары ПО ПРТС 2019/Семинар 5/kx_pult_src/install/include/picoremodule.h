/*
    PIP - Platform Independent Primitives
    Module includes
    Copyright (C) 2016  Ivan Pelipenko peri4ko@yandex.ru

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PICOREMODULE_H
#define PICOREMODULE_H

#include "pistring.h"
#include "picollection.h"
#include "piobject.h"
#include "pistatemachine.h"
#include "pitime.h"
#include "pivariant.h"
#include "picli.h"
#include "pichunkstream.h"
#include "pipropertystorage.h"

#endif // PICOREMODULE_H
