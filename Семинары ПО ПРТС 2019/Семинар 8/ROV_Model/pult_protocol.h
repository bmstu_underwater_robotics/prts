#ifndef PULT_PROTOCOL_H
#define PULT_PROTOCOL_H

#include <QObject>
#include <QTimer>
#include <QUdpSocket>

#pragma pack(push,1)

struct CoordStruct
    {
     float X;           ///---Координата X, м
     float Y;           ///---Координата Y, м (глубина)
     float y;           ///---Координата y, м (отстояние)
     float Z;           ///---Координата Z, м
     float VX;          ///---Скорость по X, м/с
     float VY;          ///---Скорость по Y, м/с
     float VZ;          ///---Скорость по Z, м/с
     float PSI;         ///---Угол курса, град (0...360˚)
     float TETA;        ///---Угол дифферента, град (-90˚...+90˚)
     float GAMMA;       ///---Угол крена, град (-90˚...+90˚)
     float Wx;      ///---Скорость поворота вокруг оси Xa, рад/с
     float Wy;      ///---Скорость поворота вокруг оси Ya, рад/с
     float Wz;      ///---Скорость поворота вокруг оси Za, рад/с
     uint checksum;
    };


struct DestParam                ///---Заданные параметры
    {
     float PSI_dest;            ///---Заданный угол курса, град (0...360˚)
     float TETA_dest;           ///---Заданный угол дифферента, град (-90˚...+90˚)
     float GAMMA_dest;          ///---Заданный угол крена, град (-90˚...+90˚)
     float DEPTH_dest;          ///---Заданная глубина, м
     float VX_dest;             ///---Заданная скорость по X, м/с
     float VY_dest;             ///---Заданная скорость по Y, м/с
     float VZ_dest;             ///---Заданная скорость по Z, м/с
     uint checksum;
    };


#pragma pack (pop)

class Pult_protocol : public QObject
{
    Q_OBJECT
public:
    DestParam from_pult;
    CoordStruct to_pult;
    explicit Pult_protocol(const QString & config = "protocols.conf", const QString & name = "pult", QObject *parent = 0);
private:
    QTimer *timer;
    QUdpSocket *receiveSocket;
    QUdpSocket *transmitSocket;

    QHostAddress ip_pult, ip_rov;
    int port_pult, port_rov;
    float frequency;

    uint checksum_i(const void * data, int size);


    bool validate(const CoordStruct & data) {
        return (data.checksum == checksum_i(&data, sizeof(data) - 4));
    }
    bool validate(const DestParam & data) {
        return (data.checksum == checksum_i(&data, sizeof(data) - 4));
    }
    void aboutSend() {
        to_pult.checksum = checksum_i(&to_pult, sizeof(to_pult) - 4);
    }


signals:

public slots:
    void sendData();
    void receiveData();
};

#endif // PULT_PROTOCOL_H
