#include "widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    //создаем класс, который осуществляет обмен по UDP
    protocol = new PultProtocol("protocols.conf", "pult");
    connect(&timer, SIGNAL(timeout()), SLOT(updateValues()));
    timer.start(1000);
}
//функция, которая считывает данные, пришедшие от модели НПА и \
    обновляет их в интерфейсе. Функция вызывается по сигналу timeout() \
    таймера timer
void Widget::updateValues() {
    curPsiLbl->setText(QString::number(protocol->rec_data.PSI));
    desPsiLbl->setText(QString::number(protocol->send_data.PSI_dest));
    if (protocol->rec_data.mode==Ruchnoi)
    {
        ruchnoiBtn->setStyleSheet("background-color:green");
        autoBtn->setStyleSheet("background-color:grey");
    }
    else {
        autoBtn->setStyleSheet("background-color:green");
        ruchnoiBtn->setStyleSheet("background-color:grey");
    }
}

//асинхронная обработка нажатия кнопок
void Widget::on_addPsiBtn_clicked() {
    protocol->send_data.PSI_dest++;
}

void Widget::on_decPsiBtn_clicked() {
    protocol->send_data.PSI_dest--;
}

void Widget::on_ruchnoiBtn_clicked() {
    protocol->send_data.mode = Ruchnoi;
}

void Widget::on_autoBtn_clicked() {
    protocol->send_data.mode = Automatiz;
}
